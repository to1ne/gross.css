---
pagetitle: gross.css --- Changelog
author: Toon Claes
keywords:
  - css
  - minimal
  - framework
  - accessibility
---

### Version 2.0.0 - Egg Carton

Second release, codename Egg Carton.

Notable changes:

- change yellowish background to gray
- softer color for text
- different text color for blockquotes
- devote less characters to table styling

Thanks to [\@drew_mc](https://twitter.com/drew_mc) for the inspiration
provided at [bettermotherfuckingwebsite.com](http://bettermotherfuckingwebsite.com/).

### Version 1.0.0 - Omelette

Initial release, codename Omelette.

Notable features:

- yellowish background
- increased font size and line height
- horizontal borders in tables and full width
