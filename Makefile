all: public

test: size-check

public: public/index.html public/CHANGELOG.html public/LICENSE.txt public/gross.css public/demo

size-check:
	@echo "Filesize: $(size)"
	@test $(size) -eq 144

public/demo: demo.html public/demo/gross.css
	$(MKDIR) -p $@
	$(CP) $< $@/index.html

public/index.html: README.md

public/%.html:
	$(MKDIR) -p $(dir $@)
	$(PANDOC) -f markdown -t html5 -s -c gross.css -o $@ $(or $<,$(patsubst %.html,%.md,$(notdir $@)))

public/%: %
	$(MKDIR) -p $(dir $@)
	$(CP) $< $@

%/gross.css: gross.css
	$(MKDIR) -p $(dir $@)
	$(CP) $< $@

clean:
	$(RM) -rf public

.PHONY: all test size-check clean
.SECONDARY:

CP ?= cp
GREP ?= grep
MKDIR ?= mkdir
PANDOC ?= pandoc --no-highlight --lua-filter=pandoc/links-to-markdown.lua
RM ?= rm
WC ?= wc

size = $(strip $(shell $(WC) -c < gross.css))
